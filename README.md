# LEARN RUBY THE EASY WAY:
[The hard one is here](http://ruby.learncodethehardway.org/book/)

HOW TO TEST:

* Get rspec
```sh
gem install rspec
```
That's easy.

* Create a spec folder and lib folders for tests and source files accordingly.
```sh
mkdir spec
mkdir lib
```
* Make a simple file that we will test later:
```sh
touch lib/first.rb
vim lib/first.rb
```
```ruby
#!/usr/bin/env ruby
def sum(a, b)
    return a+b
end
```
And we have our file written, so we can get to the tests.
* Make a simple test file and begin to edit it: 
```sh
touch spec/first_spec.rb
vim spec/first_spec.rb
```
Remember to name test file in the following way: `filename_spec.rb`
```ruby
require_relative '../lib/first' # we include our file to test

describe "#sum" do
    it "should be defined" do
        expect{sum(1,2)}.not_to raise_error # we expect our defined not to raise any errors
    end
    it "should return correct sum" do
        expect(sum(1,2)).to eq(3) # we expect sum(1,2) to return value 1+2 = 3
        expect(sum(1,2)).to be_a_kind_of Fixnum # we want the return value to be a instance of Fixnum class
        expect(sum(5,5)).not_to eq(0) # we want it not to equal some random number
    end
end
```
* We have written our main ruby file `lib/first.rb` and our test `spec/first_spec.rb` so it is time to TEST IT!!1111111 <wow>
```sh
rspec test

Finished in 0.00388 seconds (files took 0.14339 seconds to load)
1 example, 0 failures
```
So, yeah. We did it. We made our `first` program and ruby and successfully tested it with 0 failures. 
You can go know. Go on, son. Go on and code. Code as much as you have to! 

### GO CATCH SOME EXCEPTIONS

