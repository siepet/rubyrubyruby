#!/usr/bin/env ruby

require 'pp'
#require 'optparse'

#options = {}
#opt = {}
#optParse = OptionParser.new do |opts|
#    opts.banner = "Bin/Hex/Dec Conversion Tool\nUsage: systemy.rb -to bin 2048 \n"
#    opts.on("-t", "--to", "Destrination numeral system") do |t|
#        options[:to] = t
#    end
#    opts.on("-a", "--author", "Show author info") do |a|
#        options[:author] = a
#    end
#    opts.on("-v", "--version", "Show version info") do |v|
#        options[:version] = v
#    end
#    opts.on("-h", "--help", "Show help") do |h|
#       puts opts
#       exit
#    end
#end
#optParse.parse!

#if ARGV.empty?
#    puts optParse
#elsif ARGV.size > 3
#    puts "Wrong amount of arguments!"
#    exit
#end
#if options[:author]
#    puts "Author: siepet7 at gmail"
#    exit
#end
#if options[:version]
#    puts "Version: 0.01 alpha"
#    exit
#end
# to = {
#to = ARGV[0]
#number = ARGV[1]

def atoi(number = "")
    output = 0
    len = number.to_s.length
    (0..len - 1).each do |i|
        output = 10 * output + number[i].to_i
    end
    return output
end

def arrToInt(arr)
    liczba = 0
    arr.each do |num|
        liczba = liczba * 10 + num 
    end
    return liczba
end

def toBinary(number)
    number = atoi(number)
    length = number.bit_length
    outcome = []
    (0..length-1).each do |i|
        outcome.push((number >> i) & 1) 
    end
    #print "#{number} in binary:  "
    #outcome.reverse.each do |e|
    #    print e
    #end
    #puts
    ret = arrToInt(outcome.reverse)
    return ret 
end
def hexalize(arr)
  hexes = {
    [0,0,0,0] => "0",
    [0,0,0,1] => "1",
    [0,0,1,0] => "2",
    [0,0,1,1] => "3",
    [0,1,0,0] => "4",
    [0,1,0,1] => "5",
    [0,1,1,0] => "6",
    [0,1,1,1] => "7",
    [1,0,0,0] => "8",
    [1,0,0,1] => "9",
    [1,0,1,0] => "A",
    [1,0,1,1] => "B",
    [1,1,0,0] => "C",
    [1,1,0,1] => "D",
    [1,1,1,0] => "E",
    [1,1,1,1] => "F"
  }
  return hexes[arr]
end

def toHexadecimal(number)
    wynik = ""  
    int_number = atoi(number)
    bin_number = toBinary(number)
    bit_len = int_number.bit_length
    exc = 4 - (bit_len % 4)
    if exc == 4
        exc = 0
    end
    array = bin_number.to_s.split('').map {|x| x.to_i}
    total_bits = exc + bit_len
    grps = total_bits / 4
    array = array.reverse
    (0..exc-1).each do |i|
        array.push(0)
    end
    array = array.reverse
    (0..total_bits-1).step(4) do |i|
        wynik << hexalize(array[i..i+3])
    end
    #puts "#{number} in hexadecimal: #{wynik}"
    return wynik
end

