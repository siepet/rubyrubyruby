require 'simplecov'
require 'pp'
SimpleCov.start
require_relative '../lib/systemy'

describe "#atoi" do
    it "should be defined" do
        expect{atoi("10")}.not_to raise_error
    end

    it "should convert ascii to integer" do
        expect(atoi("10")).to eq(10)
    end
end

describe "#arrToInt" do
    
    it "should be defined" do
        expect{arrToInt([1, 0, 1, 0])}.not_to raise_error
    end
    
    it "should convert Array to Fixnum" do
        expect(arrToInt([1,0,1,0])).to be_a_kind_of Fixnum 
    end
    
    it "should return the correct value" do
        expect(arrToInt([1,0,1,0])).to eq(1010)
    end
end

describe "#toBinary" do

    it "should be defined" do
        expect { toBinary(216) }.not_to raise_error
    end

    it "should convert to binary" do
        expect(toBinary("10")).to eq(1010)
        expect(toBinary("2")).to eq(10)
        expect(toBinary("1024")).not_to eq(1011)
    end
end

describe "#hexalize" do
    it "should be defined" do
        expect { hexalize([0,0,0,1]) }.not_to raise_error
    end
    it "should convert array of four elements into appropriate hex symbol" do
        expect(hexalize([1,1,1,1])).to eq("F")
        expect(hexalize([0,1,0,1])).to eq("5")
        expect(hexalize([0,0,1,0])).to eq("2");
        expect(hexalize([0,0,1,1])).to eq("3");
        expect(hexalize([1,0,0,1])).to eq("9");
    end
end
describe "#toHexadecimal" do
    it "should be defined" do
        expect { toHexadecimal(0) }.not_to raise_error
    end
    it "should convert to hexadecimal" do
        expect(toHexadecimal("10")).to eq("A")
        expect(toHexadecimal("5")).to eq("5")
        expect(toHexadecimal("16")).to eq("10")
    end
end


