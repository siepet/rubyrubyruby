#!/usr/bin/env ruby

require 'optparse'

options = {}
opt = {}
optParse = OptionParser.new do |opts|
    opts.banner = "Bin/Hex/Dec Conversion Tool\nUsage: systemy.rb -to bin 2048 \n"
    opts.on("-t", "--to", "Destination numeral system") do |t|
        options[:to] = t
    end
    opts.on("-a", "--author", "Show author info") do |a|
        options[:author] = a
    end
    opts.on("-v", "--version", "Show version info") do |v|
        options[:version] = v
    end
    opts.on("-h", "--help", "Show help") do |h|
       puts opts
       exit
    end
end
optParse.parse!

if ARGV.empty?
    puts optParse
elsif ARGV.size > 3
    puts "Wrong amount of arguments!"
    exit
end
number = ARGV[1]

def atoi(number)
    output = 0
    len = number.length
    (0..len - 1).each do |i|
        output = 10 * output + number[i].to_i
    end
    return output
end
def toBinary(number)
    number = atoi(number)
    length = number.bit_length
    outcome = []
    (0..length-1).each do |i|
        outcome.push((number >> i) & 1) 
    end
    outcome.reverse.each do |e|
        print e
    end
end

def toHexadecimal(number)
    puts "from dec to hex: #{number}"
end

toBinary(number)
