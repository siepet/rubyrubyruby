#!/bin/ruby env

class Example
  require './part1'
  require './part2'
  include Enumerable
  def byf(x)
    x.sort_by_freq
  end
  def myf(x)
    x.sort_distinct_by_freq
  end
  def kyf(x)
    x.to_hist
  end
end

ex = Example.new
arr = [1, 2, 3, 4, 1, 2, 4, 8, 1, 4, 9, 16]
puts ex.byf(arr).join(", ")
puts ex.myf(arr).join(", ")
puts ex.kyf(arr)
