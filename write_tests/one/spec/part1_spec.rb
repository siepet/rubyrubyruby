require 'simplecov'
SimpleCov.start

require_relative '../lib/part1'

# TODO (refactor): modify $LOAD_PATH
# $LOAD_PATH << File.expand_path('../../lib', __FILE__)
# require 'part1'

# tutaj wpisac testy

describe "#sort_by_freq" do
  let(:dummy_class) { Class.new { include Enumerable } }
  it "should be defined" do
    expect{[].sort_by_freq}.not_to raise_error
  end
  it "should return an array" do
    expect([].sort_by_freq).to be_a_kind_of Array
  end
  it "should sort an array by frequency" do
    x = [1, 1, 1, 2, 2, 3]
    y = [1, 2, 3, 4, 1, 2, 4, 8, 1, 4, 9, 16]
    expect(x.sort_by_freq).to eq([3, 2, 2, 1, 1, 1])
    expect(y.sort_by_freq).to eq([3, 8, 9, 16, 2, 2, 1, 1, 1, 4, 4, 4])
  end
end

describe "#sort_distinct_by_freq" do
  let(:dummy_class) { Class.new { include Enumerable } } 
  it "should not raise an error" do
    expect{[].sort_distinct_by_freq}.not_to raise_error
  end
  it "should return an array" do
    expect([].sort_distinct_by_freq).to be_a_kind_of Array
  end
  it "should distinctivly sort the given array" do
    expect([1, 2, 3, 4, 1, 2, 4, 8, 1, 4, 9, 16].sort_distinct_by_freq).to eq([3, 8, 9, 16, 2, 1, 4])
  end
end
