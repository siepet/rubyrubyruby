require 'simplecov'
SimpleCov.start

require_relative '../lib/part2'

# tutaj wpisac testy
describe "#to_hist" do
  let(:dummy_class) { Class.new { include Enumerable } }
  it "should not raise an error" do
    expect{[].to_hist}.not_to raise_error
  end
  it "should return Hash" do
    expect([].to_hist).to be_a_kind_of Hash
  end
  it "should return histogram of an array in a hash" do
    expect([1, 2, 3, 4, 1, 2, 4, 8, 1, 4, 9, 16].to_hist).to eq({1=>3, 2=>2, 3=>1, 4=>3, 8=>1, 9=>1, 16=>1})
  end
end  
