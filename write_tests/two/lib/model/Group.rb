require 'csv'
require_relative './User'
class Group
  attr_accessor :name, :bio
  attr_reader :members

  def initialize(name, bio)
    @name = name
    @bio = bio
    @members = Array.new
  end

  def full?
    @members.length >= 3
  end

  def addMember(member)
    if !self.full?
      @members.push(member)
      return true
    else
      return false
    end
  end

  def findMember(member)
  end

  def delMember(member)
  end

  def makeRow
    array = [name, bio]
    @members.each do |member|
      array << "#{member.serialize}"
    end
    array
  end

  def save
    CSV.open("./groups.csv", "a") do |csv|
      csv << makeRow
    end
  end

end
