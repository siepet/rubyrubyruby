require 'csv'
class User
  attr_accessor :uid, :name, :age
  @@id = 0
  def initialize(name, age)
    @name = name
    @age = age
    @uid = @@id
    @@id = @@id + 1
  end

  def self.id
    @@id
  end

  def ofAge?
    if age > 18
      return true
    else
      return false
    end
  end

  def introduce!
    return "Hey, I am #{name} and I am #{age} years old!"
  end

  def serialize
    return [@uid, @name, @age]
  end

  def to_s
    return @uid.to_s+" / "+@name+" / "+@age.to_s
  end

end
