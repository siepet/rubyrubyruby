require 'simplecov'
SimpleCov.start
require_relative '../../lib/model/User'
require_relative '../../lib/model/Group'


describe 'Manage groups' do
  let(:group) { Group.new("Team Liquid", "Professional gaming team")}
  let(:user) { User.new("Marcin", 21) }

  context 'group' do

    it "does not raise an error" do
      expect{ group }.not_to raise_error
    end

    it "checks if group is full" do
      expect(group.full?).to eq(false)
    end

  end

  context 'adding member to a group' do

    it "adds a new member to a group" do
      expect(group.addMember(user)).to be(true)
    end

    it "throws an error when group is full" do
      expect(group.addMember(user)).to be(true)
      expect(group.addMember(user)).to be(true)
      expect(group.addMember(user)).to be(true)
      expect(group.addMember(user)).to be(false)
    end

  end

  context 'makeRow' do

    it "returns an array" do
      expect(group.makeRow).to be_a_kind_of Array
    end

  end

end
