require 'simplecov'
SimpleCov.start
require_relative '../../lib/model/Group'

describe Group do
  let(:group) { Group.new("Janusze", "Grupa z Rzeszowa") }

  it "does not raise an error" do
    expect { group }.not_to raise_error
  end

  it "initializes properly" do
    expect(group.name).to eq("Janusze")
    expect(group.bio).to eq("Grupa z Rzeszowa")
  end

  context "managing group" do
    let(:user1) { instance_double("User1", :uid => 0, :name => "Marcin", :age => 8, :serialize => "[0, 'Marcin', 8]") }
    let(:user2) { instance_double("User2", :uid => 1, :name => "Radek", :age => 17, :serialize => "[1, 'Radek', 17]") }
    let(:user3) { instance_double("User3", :uid => 2, :name => "Kasia", :age => 21, :serialize => "[2, 'Kasia', 21]") }
    let(:user4) { instance_double("User4", :uid => 3, :name => "Basia", :age => 42) }

    describe "#full?" do

      it "return false when group is not full" do
        expect(group.full?).to eq(false)
      end

      it "returns true when group is full" do
        group.addMember(user1)
        group.addMember(user2)
        group.addMember(user3)
        expect(group.full?).to eq(true)
      end

    end

    describe "#addMember" do

      it "adds member to group array" do
        expect(group.addMember(user1)).to eq(true)
      end

      it "fails to add member to full group" do
        group.addMember(user1)
        group.addMember(user2)
        group.addMember(user3)
        expect(group.addMember(user4)).to eq(false)
      end

    end

    describe "#makeRow" do

      it "its return type is type of Array" do
        expect(group.makeRow).to be_a_kind_of Array
      end

      it "makes a row for csv file with information about group" do
        group.addMember(user1)
        group.addMember(user2)
        group.addMember(user3)
        expect(group.makeRow).to eq(["Janusze", "Grupa z Rzeszowa", "[0, 'Marcin', 8]", "[1, 'Radek', 17]", "[2, 'Kasia', 21]"])
      end

    end

  end

end
