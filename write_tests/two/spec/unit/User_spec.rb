require 'simplecov'
SimpleCov.start
require_relative '../../lib/model/User'
describe User do
  let(:user) { User.new("Marcin", 21) }
  it "does not raise an error" do
    expect { user }.not_to raise_error
  end

  it "should increase id each time user is created" do
    expect{user}.to change{User.id}.from(3).to(4)
  end

  it "should return current User counter" do
    expect(User.id).to eq(4)
  end

  context "when being an user" do
    describe "#ofAge?" do
      it "returns true for users of age" do
        expect(user.ofAge?).to eq(true)
      end

      it "returns false for user not of age" do
        expect(User.new("Niepelnoletni", 11).ofAge?).to eq(false)
      end
    end

    describe "#introduce!" do
      it "should return string" do
        expect(user.introduce!).to be_a_kind_of String
      end

      it "should return correct introduction" do
        expect(user.introduce!).to eq("Hey, I am Marcin and I am 21 years old!")
      end
    end

    describe "#serialize" do
      it "should return an array" do
        expect(user.serialize).to be_a_kind_of Array
      end

      it "should return non empty array" do
        expect(user.serialize.size).to be > 0
      end

      it "serializes the user object into an array" do
        expect(user.serialize).to eq([10, "Marcin", 21])
      end
    end

    describe "#to_s" do

      it "should return string" do
        expect(user.to_s).to be_a_kind_of String
      end

      it "returns formed string from properties" do
        expect(user.to_s).to eq("12 / Marcin / 21")
      end

    end

  end

end
